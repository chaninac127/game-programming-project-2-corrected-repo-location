
--------------------------
README - FIFTY FINE LASERS
--------------------------
Version: 1.0
(c) Alpaca Sound 2015

This package contains nothing but audio files. Enjoy!



---
Contact and support: info@alpacasound.se
Website: http://www.alpacasound.se/
 
Alpaca Sound creates music and sound effects for video games. (And sometimes does a bit of coding.)
