﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanAnimationController : MonoBehaviour
{
    bool isMoving;
    public Animator animator;
    public float animationIndex;
    public ExperimentalTacticMove tacmove;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        isMoving = tacmove.isMoving();
        Debug.Log(string.Format("{0} isMoving : {1}",this.gameObject.name,isMoving));
        if (isMoving)
        {
            animator.SetFloat("AnimationIndex", 1);
        }
        else
        {
            animator.SetFloat("AnimationIndex", 0);
        }
    }
}
