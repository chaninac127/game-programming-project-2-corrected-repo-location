﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinningScene : MonoBehaviour
{

    [SerializeField] Text winnerText;
    public string winner = "";


    // Start is called before the first frame update
    void Start()
    {
        winner = FindObjectOfType<StateKeeper>().Winner;
        winnerText.text = winner;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
