﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootingMechanics : MonoBehaviour
{
    [SerializeField] Weapon weapon;
    [SerializeField] AudioClip[] HitSound;
    [SerializeField] AudioClip deathSFX;
    [SerializeField] string enemyTag;
    [SerializeField] Text healthText;
    [SerializeField] int health;
    [SerializeField] int maxHealth = 12;
    [SerializeField] Image healthbar;
    [SerializeField] int range = 5;
    [SerializeField] int damage = 6;
    [SerializeField] Text ChanceToHit;

    private Color myColor;
    public List<GameObject> targetable;
    public bool shotted;

    public void getTargetable()
    {
        targetable = new List<GameObject>();
        ShootingMechanics[] en = FindObjectsOfType<ShootingMechanics>();
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        foreach (GameObject enemy in enemies)
        {
            RaycastHit hit;
            if (Physics.Linecast(transform.position, enemy.transform.position, out hit))
            {
                if (hit.transform.tag == enemyTag && Vector3.Distance(transform.position,enemy.transform.position)<range)
                {
                    targetable.Add(enemy);
                }
            }
        }
    }

    private void updateHitChance(float chance)
    {
        ChanceToHit.text = string.Format("Chance: {0}%", (int)(chance * 100.0));
    }

    private void disableHitChance()
    {
        ChanceToHit.text = "";
    }

    private float calculateHitchance(GameObject enemy)
    {
        float distance = Vector3.Distance(transform.position, enemy.transform.position);
        float hitChance = 1.25f-(distance/(float)range);
        //Debug.Log("Distance : " + distance);
        //Debug.Log("Hit chance (range/distance) : " + hitChance);
        return hitChance;
    }

    public void EnterShootingPhase()
    {
        shotted = false;
        getTargetable();
        //Debug.Log(targetable.Count);

        foreach (GameObject enemy in targetable)
        {
            enemy.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
            float hitChance = calculateHitchance(enemy);
            enemy.GetComponent<ShootingMechanics>().updateHitChance(hitChance);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            AudioClip shootingSfx = weapon.getShootingSfx();
            this.gameObject.GetComponent<AudioSource>().clip = shootingSfx;
            this.gameObject.GetComponent<AudioSource>().Play();
            //.PlayOneShot(shootingSfx);
            //AudioClip reloadingSfx = weapon.getReloadingSfx();
            //this.gameObject.GetComponent<AudioSource>().PlayOneShot(reloadingSfx);
            if (Physics.Raycast(ray, out hit, 100))
            {
                attack(hit.transform.gameObject);
            }
            foreach (GameObject enemy in targetable)
            {
                if (enemyTag == "Team1")
                {
                    enemy.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                }
                else
                {
                    enemy.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                }
                enemy.GetComponent<ShootingMechanics>().disableHitChance();
            }
            
            shotted = true;
        }
    }

    public void playHitSfx()
    {
        this.gameObject.GetComponent<AudioSource>().PlayOneShot(HitSound[(int)Random.Range(0,HitSound.Length)]);
    }

    public void attack(GameObject target)
    {
        float hitChance = calculateHitchance(target);
        //System.Random rng = new System.Random();
        if(Random.Range(0.0f, 1.0f) < hitChance)
        {
            if (target.GetComponent<ShootingMechanics>() != null)
            {
                target.GetComponent<ShootingMechanics>().playHitSfx();
                target.GetComponent<ShootingMechanics>().dealDamage(damage);
            }
            else
            {
                ExperimentalTurnManager.EndTurn();
            }
        }
    }
    public void dealDamage(int damage)
    {
        Debug.Log(enemyTag);
        if (health > damage)
        {
            health -= damage;
            healthText.text = health.ToString();
            healthbar.fillAmount=(float)health/(float)maxHealth;
            //healthText.
        }
        else
        {
            health = 0;
            healthText.text = health.ToString();
            healthbar.fillAmount = 0;
            dead();
        }

    }
    void dead()
    {
        GetComponent<AudioSource>().PlayOneShot(deathSFX);
        ExperimentalTurnManager.RemoveUnit(this.gameObject.GetComponent<ExperimentalTacticMove>());
        Destroy(this.gameObject,1.0f);
    }

    //// Start is called before the first frame update
    void Start()
    {
        ChanceToHit.text = "";
        health = maxHealth;
        healthText.text = health.ToString();
        shotted = false;
        if(this.gameObject.tag == "Team1")
        {
            enemyTag = "Team2";
        }
        else
        {
            enemyTag = "Team1";
        }
    }

    //// Update is called once per frame
    //void Update()
    //{

    //}
}
