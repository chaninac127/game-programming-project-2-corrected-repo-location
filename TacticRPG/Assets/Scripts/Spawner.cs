﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] Transform[] redTeamSpawnLocation;
    [SerializeField] Transform[] blueTeamSpawnLocation;
    //public GameObject[] redClone;
    //public GameObject[] blueClone;
    int[] team1Comp;
    int[] team2Comp;
    [SerializeField]GameObject[] blueTeam;
    [SerializeField]GameObject[] redTeam;
    StateKeeper sk;

    // Start is called before the first frame update
    void Start()
    {
        sk = FindObjectOfType<StateKeeper>();
        team1Comp = sk.getTeam1Comp();
        team2Comp= sk.getTeam2Comp();
        //generateGameObject();
        spawn(redTeamSpawnLocation,redTeam,team2Comp);
        spawn(blueTeamSpawnLocation, blueTeam,team1Comp);
    }
    private void spawn(Transform[] locations, GameObject[] team,int[] comp)
    {
        int i = 0;
        foreach(Transform t in locations)
        {
            Instantiate(team[comp[i++]], t.transform.position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
