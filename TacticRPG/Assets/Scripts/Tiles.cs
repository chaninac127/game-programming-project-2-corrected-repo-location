﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiles : MonoBehaviour
{
    public bool passable = true;
    public bool current = false;
    public bool target = false;
    public bool selectable = false;

    public bool visited = false;
    public Tiles parent = null;
    public int distance = 0;

    public List<Tiles> neighbors = new List<Tiles>();

    public void Reset()
    {
        neighbors.Clear();

        current = false;
        target = false;
        selectable = false;

        visited = false;
        parent = null;
        distance = 0;
    }

    public void FindNeighbors(float jumpHeight)
    {
        Reset();

        CheckTiles(Vector3.forward, jumpHeight);
        CheckTiles(Vector3.back, jumpHeight);
        CheckTiles(Vector3.left, jumpHeight);
        CheckTiles(Vector3.right, jumpHeight);
        
    }

    public void CheckTiles(Vector3 direction, float jumpHeight)
    {
        Vector3 halfExtends = new Vector3(0.25f,(1 + jumpHeight)/2.0f,0.25f);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends);
        
        foreach (Collider item in colliders)
        {
            Tiles tile = item.GetComponent<Tiles>();
            if(tile != null && tile.passable)
            {
                RaycastHit hit;

                if(!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1))
                {
                    neighbors.Add(tile);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (current)
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
        else if (target)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (selectable)
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
