﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSelectionScene()
    {
        SceneManager.LoadScene("CharacterPickingScene");
    }

    public void LoadStartScene()
    {
        FindObjectOfType<StateKeeper>().ResetGame();
        SceneManager.LoadScene(0);
    }

    public void LoadEndgameScene()
    {
        Debug.Log("OMAE WA MOU SHINDEIRU");
        SceneManager.LoadScene("WinningScene"); //change this
    }

    public void doExitGame()
    {
        Application.Quit();
    }

    public void loadMap()
    {
        int index = FindObjectOfType<MapSelector>().getSelectedMap();
        if(index == 0)
        {
            SceneManager.LoadScene("TownMap"); // change this to townmap index
        }else if(index == 1)
        {
            SceneManager.LoadScene("RoomsMap"); // change this to roommap index
        }
        else
        {
            Debug.Log("Invalid scene index to load");
        }
    }
}
