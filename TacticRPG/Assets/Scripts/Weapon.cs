﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    [SerializeField] AudioClip[] shootingSfx;
    [SerializeField] AudioClip[] reloadSfx;

    public AudioClip getShootingSfx()
    {
        return shootingSfx[(int)Random.Range(0, shootingSfx.Length)];
    }

    public AudioClip getReloadingSfx()
    {
        return reloadSfx[(int)Random.Range(0, shootingSfx.Length)];
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
