﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    //public GameObject Target;
    //public Vector3 targetPos;
    public Camera mainCam;
    // Use this for initialization
    void Start()
    {
        //targetPos = Camera.main.WorldToScreenPoint(Target.transform.position);
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 v = cameraToLookAt.transform.position - transform.position;
        //v.x = v.z = 0.0f;
        //transform.LookAt(cameraToLookAt.transform.position - v);
        //transform.Rotate(0, 180, 0);
        transform.LookAt(transform.position + mainCam.transform.rotation * Vector3.forward,
         mainCam.transform.rotation * Vector3.up);
    }
}