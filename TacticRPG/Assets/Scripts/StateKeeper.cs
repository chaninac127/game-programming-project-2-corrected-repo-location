﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateKeeper : MonoBehaviour
{
    public string Winner = "";
    public int[] Team1Composition = new int[3] { 0, 0, 0 }; // 0 is None, 1 is Rifle, 2 is Vanguard(shotgun), 3 is Heavy, 4 is Sniper
    public int[] Team2Composition = new int[3] { 0, 0, 0 };


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EditTeam1Comp(int index, int charac)
    {
        Team1Composition[index] = charac;
    }

    public void EditTeam2Comp(int index, int charac)
    {
        Team2Composition[index] = charac;
    }

    public void updateTeam1comp(int[] teamComp)
    {
        Team1Composition = teamComp;
    }

    public void updateTeam2comp(int[] teamComp)
    {
        Team2Composition = teamComp;
    }

    public int[] getTeam1Comp()
    {
        return Team1Composition;
    }

    public int[] getTeam2Comp()
    {
        return Team2Composition;
    }

    public void updateWinner(string team)
    {
        Winner = team;
    }

    private void Awake()
    {
        int stateKeeperCount = FindObjectsOfType<StateKeeper>().Length;

        if (stateKeeperCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ResetGame()
    {
        Debug.Log("GAME RESET");
        Destroy(gameObject);
    }
}
