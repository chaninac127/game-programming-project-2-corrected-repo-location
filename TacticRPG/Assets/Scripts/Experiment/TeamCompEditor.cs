﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamCompEditor : MonoBehaviour
{
    StateKeeper statekeeper;
    List<string> classesDropdown = new List<string>() {"Rifleman","Vanguard","Heavy","Sniper"};
    public Dropdown dropdown;
    [SerializeField] int team = 0;
    [SerializeField] int teamIndex = 0;
    [SerializeField] int selected = 0;

    // Start is called before the first frame update
    void Start()
    {
        statekeeper = FindObjectOfType<StateKeeper>();
        PopulateList();
    }

    void PopulateList()
    {
        dropdown.AddOptions(classesDropdown);
    }

    public bool checkIfSelected()
    {
        return !(selected == 0);
    }

    public void dropDownIndexChanged(int index)
    {
        if(team == 1)
        {
            statekeeper.EditTeam1Comp(teamIndex, index);
        }
        else if(team == 2)
        {
            statekeeper.EditTeam2Comp(teamIndex, index);
        }
        else
        {
            Debug.Log("Invalid team number");
        }
        int[] team1 = statekeeper.getTeam1Comp();
        int[] team2 = statekeeper.getTeam2Comp();
        Debug.Log("Team1 :");
        foreach(int i in team1)
        {
            Debug.Log(i);
        }
        Debug.Log("Team2 :");
        foreach (int i in team2)
        {
            Debug.Log(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
