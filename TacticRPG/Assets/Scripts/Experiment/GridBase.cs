﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBase : MonoBehaviour
{
    public bool passable = true;
    public bool current = false;
    public bool target = false;
    public bool selectable = false;

    public bool coverFront = false;
    public bool coverBack = false;
    public bool coverLeft = false;
    public bool coverRight = false;

    public bool visited = false;
    public GridBase parent = null;
    public int distance = 0;

    public List<GridBase> neighbors = new List<GridBase>();

    public void Reset()
    {
        neighbors.Clear();

        current = false;
        target = false;
        selectable = false;

        visited = false;
        parent = null;
        distance = 0;
    }

    public void FindNeighbors(float jumpHeight)
    {
        Reset();

        CheckTiles(Vector3.forward, jumpHeight);
        CheckTiles(Vector3.back, jumpHeight);
        CheckTiles(Vector3.left, jumpHeight);
        CheckTiles(Vector3.right, jumpHeight);

    }

    public void CheckTiles(Vector3 direction, float jumpHeight)
    {
        Vector3 halfExtends = new Vector3(0.25f, (1 + jumpHeight) / 2.0f, 0.25f);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends);

        foreach (Collider item in colliders)
        {
            GridBase gb = item.GetComponent<GridBase>();

            bool canPass = true;

            if (gb != null)
            {
                if (Vector3.Equals(direction, Vector3.forward))
                {
                    canPass = !coverFront && !gb.coverBack;
                }
                else if (Vector3.Equals(direction, Vector3.back))
                {
                    canPass = !coverBack && !gb.coverFront;
                }
                else if (Vector3.Equals(direction, Vector3.left))
                {
                    canPass = !coverLeft && !gb.coverRight;
                }
                else if (Vector3.Equals(direction, Vector3.right))
                {
                    canPass = !coverRight && !gb.coverLeft;
                }
            }

            if (gb != null && gb.passable && canPass)
            {
                RaycastHit hit;

                if (!Physics.Raycast(gb.transform.position, Vector3.up, out hit, 1))
                {
                    neighbors.Add(gb);
                }
            }
        }
    }

    void Update()
    {
        if (current)
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
        else if (target)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (selectable)
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
