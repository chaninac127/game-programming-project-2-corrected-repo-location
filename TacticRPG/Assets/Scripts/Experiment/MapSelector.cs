﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapSelector : MonoBehaviour
{

    List<string> mapDropdown = new List<string>() { "Town", "Rooms" };
    [SerializeField] int selected = 0;
    public Dropdown dropdown;

    // Start is called before the first frame update
    void Start()
    {
        PopulateList();
    }

    void PopulateList()
    {
        dropdown.AddOptions(mapDropdown);
    }

    public int getSelectedMap()
    {
        return selected;
    }

    public void dropDownIndexChanged(int index)
    {
        selected = index;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
