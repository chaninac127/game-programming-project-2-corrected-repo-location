﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentalPlayerMove : ExperimentalTacticMove
{


    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (!turn)
        {
            return;
        }

        if (!moving && !moved )
        {
            FindSelectableTiles();
            CheckMouse();
        }
        else
        {
            Move();
        }
        if (moved)
        {
            ShootingMechanics shooting = GetComponent<ShootingMechanics>();
            shooting.EnterShootingPhase();
            shooting.getTargetable();
            if ((shooting.shotted || shooting.targetable.Count == 0))
            {
                ExperimentalTurnManager.EndTurn();
            }
        }
    }
    void CheckMouse()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Tile")
                {
                    GridBase t = hit.collider.GetComponent<GridBase>();
                    if (t.selectable)
                    {
                        MoveToTile(t);
                    }
                }
            }
        }
    }
}
