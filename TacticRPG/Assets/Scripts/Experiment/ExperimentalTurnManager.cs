﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperimentalTurnManager : MonoBehaviour
{
    static Dictionary<string, List<ExperimentalTacticMove>> units = new Dictionary<string, List<ExperimentalTacticMove>>();
    static Queue<string> turnKey = new Queue<string>();
    static Queue<ExperimentalTacticMove> turnTeam = new Queue<ExperimentalTacticMove>();
    //these two are for displaying current team on the scene
    [SerializeField] Text currentTeamText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (turnTeam.Count == 0)
        {
            InitTeamTurnQueue();
        }
    }

    static void InitTeamTurnQueue()
    {
        List<ExperimentalTacticMove> teamList = units[turnKey.Peek()];

        foreach (ExperimentalTacticMove unit in teamList)
        {
            turnTeam.Enqueue(unit);
        }

        StartTurn();
    }

    public static void StartTurn()
    {
        if (turnTeam.Count > 0)
        {
            turnTeam.Peek().BeginTurn();
        }
    }

    public static string getCurrentTeam()
    {
        return turnKey.Peek();
    }

    public static void EndTurn()
    {
        ExperimentalTacticMove unit = turnTeam.Dequeue();
        unit.EndTurn();

        if (turnTeam.Count > 0)
        {
            StartTurn();
        }
        else
        {
            string team = turnKey.Dequeue();
            turnKey.Enqueue(team);
            InitTeamTurnQueue();
        }
    }

    public static void AddUnit(ExperimentalTacticMove unit)
    {
        List<ExperimentalTacticMove> list;
        if (!units.ContainsKey(unit.tag))
        {
            list = new List<ExperimentalTacticMove>();
            units[unit.tag] = list;

            if (!turnKey.Contains(unit.tag))
            {
                turnKey.Enqueue(unit.tag);
            }
        }
        else
        {
            list = units[unit.tag];
        }

        list.Add(unit);
    }

    public static void RemoveUnit(ExperimentalTacticMove unit)
    {

        List<ExperimentalTacticMove> list = units[unit.tag];

        list.Remove(unit);
        Debug.Log(string.Format("{0}'s team have {1} member(s) left", unit.tag, list.Count));

        if (list.Count <= 0)
        {
            Debug.Log("Turn Manager : Team Removed (member <= 0)");
            DeclareLoser(unit.tag);
        }

    }

    public static void DeclareLoser(string teamTag)
    {
        
        //units.Remove(teamTag);
        StateKeeper sk = FindObjectOfType<StateKeeper>();
        if (turnKey.Contains(teamTag))
        {
            string nextTeam = turnKey.Peek();
            if (nextTeam == teamTag)
            {
                turnKey.Dequeue();
            }
        }
        sk.updateWinner(turnKey.Peek());
        FindObjectOfType<SceneLoader>().LoadEndgameScene();
    }
}
