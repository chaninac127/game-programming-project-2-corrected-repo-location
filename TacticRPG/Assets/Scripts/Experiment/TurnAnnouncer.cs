﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TurnAnnouncer : MonoBehaviour
{
    public string currentTeam = "";
    [SerializeField] Text currentTeamText;


    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        currentTeam = ExperimentalTurnManager.getCurrentTeam();
        currentTeamText.text = string.Format("{0}'s turn", currentTeam);
    }
}
