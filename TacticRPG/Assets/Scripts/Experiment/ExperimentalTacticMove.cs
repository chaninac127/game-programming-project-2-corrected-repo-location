﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentalTacticMove : MonoBehaviour
{
    public bool moved = false;
    public bool turn = false;

    [SerializeField] AudioClip callout;
    List<GridBase> selectableTiles = new List<GridBase>();
    GameObject[] tiles;

    Vector3 jumpTarget;

    Stack<GridBase> path = new Stack<GridBase>();
    GridBase CurrentTile;

    bool fallingDown = false;
    bool jumpingUp = false;
    bool movingEdge = false;

    public bool moving = false;
    public int move = 5;
    public float jumphight = 2;
    public float moveSpeed = 2;
    public float jumpVelocity = 4.5f;

    Vector3 velocity = new Vector3();
    Vector3 heading = new Vector3();

    float halfheight = 0;

    public bool isMoving()
    {
        return moving;
    }

    public void Init()
    {
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        halfheight = GetComponent<Collider>().bounds.extents.y;

        Debug.Log(string.Format("Adding {0} into {1} team", gameObject.name, gameObject.tag));

        ExperimentalTurnManager.AddUnit(this);
    }

    public void GetCurrentTile()
    {
        CurrentTile = GetTargetTile(gameObject);
        CurrentTile.current = true;
    }

    public GridBase GetTargetTile(GameObject target)
    {
        RaycastHit hit;
        GridBase tile = null;

        if (Physics.Raycast(target.transform.position, -Vector3.up, out hit, 1))
        {
            tile = hit.collider.GetComponent<GridBase>();
        }
        return tile;
    }

    public void ComputeAdjacencyLists()
    {
        foreach (GameObject tile in tiles)
        {
            GridBase t = tile.GetComponent<GridBase>();
            t.FindNeighbors(jumphight);
        }
    }

    public void FindSelectableTiles()
    {
        ComputeAdjacencyLists(); //compute the whole map tile neighbors
        GetCurrentTile(); //get the current tile of the moving gameObject
        Queue<GridBase> process = new Queue<GridBase>();

        process.Enqueue(CurrentTile);
        CurrentTile.visited = true;
        while (process.Count > 0)
        {
            GridBase t = process.Dequeue();

            selectableTiles.Add(t);
            t.selectable = true;

            if (t.distance < move)
            {
                foreach (GridBase tile in t.neighbors)
                {
                    if (!tile.visited)
                    {
                        tile.parent = t;
                        tile.visited = true;
                        tile.distance = 1 + t.distance;
                        process.Enqueue(tile);
                    }
                }
            }
        }
    }

    public void MoveToTile(GridBase tile)
    {
        path.Clear();
        tile.target = true;
        moving = true;

        GridBase next = tile;
        while (next != null)
        {
            path.Push(next);
            next = next.parent;
        }

    }

    void CalculateHeading(Vector3 target)
    {
        heading = target - transform.position;
        heading.Normalize();

    }

    void SetHorizontalVelocity()
    {
        velocity = heading * moveSpeed;
    }

    public void Move()
    {
        if (path.Count > 0)
        {
            GridBase t = path.Peek();
            Vector3 target = t.transform.position;

            target.y += halfheight + t.GetComponent<Collider>().bounds.extents.y;

            if (Vector3.Distance(transform.position, target) >= 0.05f)
            {
                bool jump = transform.position.y != target.y;

                if (jump)
                {
                    Jump(target);
                }
                else
                {
                    CalculateHeading(target);
                    SetHorizontalVelocity();
                }

                transform.forward = heading;
                transform.position += velocity * Time.deltaTime;
            }
            else
            {
                transform.position = target;
                path.Pop();
            }

        }
        else
        {
            RemoveSelectableTiles();
            moving = false;
            moved = true;
        }
    }

    void Jump(Vector3 target)
    {
        if (fallingDown)
        {
            FallDownward(target);
        }
        else if (jumpingUp)
        {
            JumpUpward(target);
        }
        else if (movingEdge)
        {
            MoveToEdge();
        }
        else
        {
            PrepareJump(target);
        }
    }

    void PrepareJump(Vector3 target)
    {
        float targetY = target.y;

        target.y = transform.position.y;

        CalculateHeading(target);

        if (transform.position.y > targetY)
        {
            //Debug.Log(string.Format("Move to drop down",));
            fallingDown = false;
            jumpingUp = false;
            movingEdge = true;

            jumpTarget = transform.position + (target - transform.position) / 2.0f;
        }
        else
        {
            fallingDown = false;
            jumpingUp = true;
            movingEdge = false;

            velocity = heading * moveSpeed / 3.0f;
            float difference = targetY - transform.position.y;

            velocity.y = jumpVelocity * (0.5f + difference / 2.0f);
        }
    }

    void FallDownward(Vector3 target)
    {
        velocity += Physics.gravity * Time.deltaTime;

        if (transform.position.y <= target.y)
        {
            fallingDown = false;
            jumpingUp = false;
            movingEdge = false;

            Vector3 p = transform.position;
            p.y = target.y;
            transform.position = p;

            velocity = new Vector3();
        }
    }

    void JumpUpward(Vector3 target)
    {
        velocity += Physics.gravity * Time.deltaTime;

        if (transform.position.y > target.y)
        {
            jumpingUp = false;
            fallingDown = true;
        }
    }

    void MoveToEdge()
    {
        if (Vector3.Distance(transform.position, jumpTarget) >= 0.05f)
        {
            SetHorizontalVelocity();
        }
        else
        {
            movingEdge = false;
            fallingDown = true;

            velocity /= 5.0f;
            velocity.y = 1.5f;
        }
    }

    private void RemoveSelectableTiles()
    {
        if (CurrentTile != null)
        {
            CurrentTile.current = false;
            CurrentTile = null;
        }
        foreach (GridBase tile in selectableTiles)
        {
            tile.Reset();
        }
        selectableTiles.Clear();
    }

    public void BeginTurn()
    {
        this.gameObject.GetComponent<AudioSource>().PlayOneShot(callout);
        turn = true;
        moved = false;
    }

    public void EndTurn()
    {
        turn = false;
        moving = false;
    }
}
